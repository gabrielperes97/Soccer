#include <iostream>
#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <list>
#include <string.h>
#include <algorithm> 

#ifdef linux || LINUX || Linux || UNIX
    #define FLUSH __fpurge(stdin);
#elif defined WIN32 || Win32 || win32 || WIN_NT
    #define FLUSH fflush(stdin);
#else
    #define FLUSH printf("Esse humilde trabalho de faculdade nao foi feito pra rodar aq, tente com linux :)\n");
#endif

using namespace std;

typedef struct team
{
	char name[30];
	int points=0;
	int gamesPlayed=0;
	int wins=0;
	int ties=0;
	int loses=0;
	int goalsScored=0;
	int goalsAgainst=0;
} Team;

typedef struct game
{
	Team *team1;
	Team *team2;
	int goalsTeam1;
	int goalsTeam2;
} Game;

typedef struct touneament{
	char name[100];
	list <Team> teams;
	list <Game> games;
} Tourneament;

bool compareTeams (const team& first, const team& second)
{
	if (first.points > second.points)
		return true;
	else
	{
		if (first.points == second.points)
		{
			if (first.wins > second.wins)
				return true;
			else
			{
				if (first.wins == second.wins)
				{
					int goalDifference1 = first.goalsScored-first.goalsAgainst;
					int goalDifference2 = second.goalsScored-second.goalsAgainst;
					if (goalDifference1 > goalDifference2)
						return true;
					else
					{
						if (goalDifference1 = goalDifference2)
						{
							if (first.goalsScored > second.goalsScored)
								return true;
							else
							{
								if (first.goalsScored == second.goalsScored)
								{
									if (first.gamesPlayed < second.gamesPlayed)
										return true;
									else
									{
										if (first.gamesPlayed == second.gamesPlayed)
										{
											//testa ordem lexicografica
											if (strcmp(first.name, second.name) < 0) //first vem antes de second
											{
												return true;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}  
	return false;
}

int main()
{
	int numTourns;
	cin >> numTourns;
	list <Tourneament> tourneaments;
	for (int i=0; i<numTourns; i++)
	{
		Tourneament torn;
		FLUSH;
		cin.getline(torn.name, 100);

		int numTimes;
		cin >> numTimes;
		for (int j=0; j<numTimes; j++)
		{
			Team t;
			FLUSH;
			cin.getline(t.name, 30);
			torn.teams.push_back(t);
		}

		int numJogos;
		FLUSH;
		cin >> numJogos;
		for (int j=0; j<numJogos; j++)
		{
			Game g;
			char team1[30];
			char team2[30];
			char ent[100];
			char num[10];
			FLUSH;
			cin.getline(ent, 100);
			int k=0;
			int t=0;
			while (ent[k] !=  '#')
			{
				team1[t] = ent[k];
				t++;
				k++;
			}
			team1[t] = '\0';
			t=0;
			k++;
			while(ent[k] != '@')
			{
				num[t] = ent[k];
				t++;
				k++;
			}
			num[t] = '\0';
			g.goalsTeam1 = atoi(num);
			t=0;
			k++;
			while(ent[k] != '#')
			{
				num[t] = ent[k];
				t++;
				k++;
			}
			num[t] = '\0';
			g.goalsTeam2 = atoi(num);
			t=0;
			k++;
			while(ent[k] != '\n' && ent[k] != '\0')
			{
				team2[t] = ent[k];
				t++;
				k++;
			}
			team2[t] = '\0';
			
			for (list<Team>::iterator it=torn.teams.begin(); it != torn.teams.end(); ++it)
			{
				if (strcmp(it->name, team1) == 0)
					g.team1 = &(*it);
				else
					if (strcmp(it->name, team2) == 0)
						g.team2 = &(*it);
			}

			g.team1->gamesPlayed++;
			g.team2->gamesPlayed++;
			g.team1->goalsScored += g.goalsTeam1;
			g.team2->goalsScored += g.goalsTeam2;
			g.team1->goalsAgainst += g.goalsTeam2;
			g.team2->goalsAgainst += g.goalsTeam1;

			//wins ties and lost
			if (g.goalsTeam1 > g.goalsTeam2)
			{
				g.team1->points += 3;
				g.team1->wins++;
				g.team2->loses++;
			}else
			{
				if (g.goalsTeam2 > g.goalsTeam1)
				{
					g.team2->points += 3;
					g.team2->wins++;
					g.team1->loses++;
				}
				else
				{
					g.team1->points += 1;
					g.team2->points += 1;
					g.team1->ties++;
					g.team2->ties++;
				}
			}
			 


			cout << team1 << " " << g.goalsTeam1 << " " << g.goalsTeam2 << " " << team2 << "\n";
			torn.games.push_back(g);

		}

		tourneaments.push_back(torn);
	}

	for (list<Tourneament>::iterator torn=tourneaments.begin(); torn != tourneaments.end(); ++torn)
	{
		torn->teams.sort(compareTeams); //Ordena a lista de times usando a função compare teams como referencia para troca
		//a função sort é NlogN
	}


	for (list<Tourneament>::iterator torn=tourneaments.begin(); torn != tourneaments.end(); ++torn)
	{
		cout << torn->name << endl;
		int i=1;
		for (list<Team>::iterator it=torn->teams.begin(); it != torn->teams.end(); ++it)
		{
			cout << i << ") " << it->name << " " << it->points <<  "p, " << it->gamesPlayed << "g (" << 
				it->wins << "-" << it->ties << "-" << it->loses << "), " << it->goalsScored-it->goalsAgainst << 
				"gd (" << it->goalsScored << "-" << it->goalsAgainst << ")" << endl;
			i++;
		}
		cout << endl;
	}



}